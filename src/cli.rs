mod domain;
//use socket2::{Domain,Type,Socket,SockAddr};
use std::fs::remove_file;
use std::process::exit;


const CLI_ADDRESS: &str = "/tmp/cli";
const SRV_ADDRESS: &str = "/tmp/srv";

fn main() -> std::io::Result<()> {
   
    let socket = domain::socket_open(CLI_ADDRESS)?;
    domain::socket_fill(Some(SRV_ADDRESS))?;

     let msg = "Message From Client".as_bytes();

     match domain::socket_write(&socket,msg,SRV_ADDRESS) {
         Ok(result) => result,
         Err(_) => {println!("Unable to connect to server! Make sure it is running");
                    remove_file(CLI_ADDRESS)?;
                    exit(0)
        }

     }

     let mut buffer = [0; 512];
     domain::socket_read(&socket,&mut buffer)?;
    
    remove_file(CLI_ADDRESS)?;

    Ok(())
}
