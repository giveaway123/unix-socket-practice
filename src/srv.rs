mod domain;
use std::fs::remove_file;

const SRV_ADDRESS: &str = "/tmp/srv";

fn main() -> std::io::Result<()> {
    

    let socket = domain::socket_open(SRV_ADDRESS)?;
    
    let mut buffer = [0; 512];
    println!("Waiting for client........");
    let cli_addr = domain::socket_read(&socket, &mut buffer)?;
    
    let to_client = "Server ack";
    let cli_str = cli_addr.as_pathname().unwrap().to_string_lossy();
    domain::socket_write(&socket,to_client.as_bytes(),&cli_str)?;

    remove_file(SRV_ADDRESS)?;

    Ok(())
}
