Unix Socket Practice Interprocess Communication

Learn more about unix sockets here: https://en.wikipedia.org/wiki/Unix_domain_socket


How to run (mac/linux/wsl or unix-like OS)

Install rust from https://rustup.rs/
In the root directory of the repo run command 'cargo run' to start the "server". The server will wait for "client"
In a separate tab or window of terminal run command 'cargo run --bin cli' to start the "client"
'Client' message should displayed on 'server' terminal and 'server' acknowledge message on 'client' terminal
