use std::io;
use socket2::{Domain,Type,Socket,SockAddr};
use std::os::unix::net::UnixDatagram;

pub fn socket_fill(file: Option<&str>) -> Result<SockAddr,io::Error> {
    if file.is_none() {
        return Err(io::Error::from_raw_os_error(2));
    } 
    let addr = SockAddr::unix(file.unwrap())?;
    Ok(addr)
}

pub fn socket_open(file: &str) -> std::io::Result<UnixDatagram> {
    let socket = Socket::new(Domain::unix(),Type::dgram(),None)?;
    let addr =  socket_fill(Some(file))?;
    socket.bind(&addr)?;

    let s = socket.into_unix_datagram();
        
    Ok(s)
}

pub fn socket_read(socket: &UnixDatagram, buffer: &mut [u8; 512]) -> std::io::Result<std::os::unix::net::SocketAddr> {
     
    let (_,cli_addr) = socket.recv_from(buffer)?;
    let data = String::from_utf8(buffer.to_vec()).unwrap();
    println!("{}",data);

    Ok(cli_addr)
}

pub fn socket_write(socket: &UnixDatagram, buffer: &[u8],dest: &str) -> std::io::Result<()> {
    
  // match socket.send_to(buffer, dest) {
  //      Ok(result) => result,
  //      Err(_) => {println!("Unable to connect to server! Make sure it is running");
  //                  exit(0)
  //      }
  //  }; 
    socket.send_to(buffer,dest)?;

    Ok(())
}
